## Ontology for Biological Investigations - With Applications for nPOD (OBI-WAN)

OBI-WAN is an application-specific and "glue" ontology that builds upon the OBI ontology to support data annotation and creation of open linked data for the [Network of Pancreatic Organ Donors (nPOD)](https://npod.org/). It provides the data model for the knowledgebase called the **nPOD DataGraph**, which aims to describe all *reusable* nPOD-related data that has ever been produced. 

### Motivation for the development of OBI-WAN (long version)

The Network for Pancreatic Organ Donors (nPOD), est. 2009, is a biobank that distributes organ tissues world-wide for researchers to conduct assays that help answer important and often diverse biological questions on Type 1 & Type 2 diabetes.

In 2016, the FAIR sharing principles were published and emphasis grew on the merits of data integration and sharing, thus a great effort was proposed to retrospectively curate datasets that have been generated using the nPOD samples. The data would be annotated and standardized to facilitate secondary analyses or other re-use. OBI was naturally chosen as the source of terms for annotating the collected datasets given its suitability and established use in numerous other open biomedical projects. OBI-WAN does interoperate with other ontologies in the Open Biomedical Ontologies ecosystem.

Aside from being a "glue" ontology, OBI-WAN conceptualizes new concepts and hierarchies that better service the nPOD domain (which may be undefined or poorly defined elsewhere). For example, custom Theme classes reflect the historical themes of nPOD working groups. For database scalability the ontology stays closest to OWL-RL profile.

**Visualize and explore the ontology here: https://npod.gitlab.io/obi-wan**

### Products

- `obi-wan.ttl` : Ontology in .ttl format.
- `obi-wan.owl` : Ontology in OWL (XML) format.

### Other visualization options

Load the ontology into [Protege](https://protege.stanford.edu/) ontology editor 

### Acknowledgements

We would like to acknowledge the group who developed the WebVowl visualization tool:
 
Lohmann, S., Negru, S., Haag F., Ertl, T.: Visualizing Ontologies with VOWL. Semantic Web 7(4): 399-419 (2016)



